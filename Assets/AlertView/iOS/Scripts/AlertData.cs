﻿using System;
using System.Collections.Generic;

namespace AlertView.iOS
{
    [Serializable]
    public class AlertData
    {
        public string title;
        public string message;
        public AlertStyle style;
        public List<ButtonData> buttons;


        public AlertData(string title, string message, AlertStyle style, List<ButtonData> buttons)
        {
            this.title = title;
            this.message = message;
            this.style = style;
            this.buttons = buttons;
        }
    }
}