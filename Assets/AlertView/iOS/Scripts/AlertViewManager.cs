﻿using System;
using System.Collections.Generic;
#if UNITY_IOS && !UNITY_EDITOR
using System.Linq;
using System.Runtime.InteropServices;
using AOT;
#endif
using UnityEngine;

namespace AlertView.iOS
{
    public static class AlertViewManager
    {
        public delegate void intCallbackFunc(int number);
        
        #if UNITY_IOS && !UNITY_EDITOR
        
        [DllImport("__Internal")]
        private static extern void _ShowMultipleeButtonsAlertView(string title, string message, string[] buttonsTexts, int[] buttonStyles, intCallbackFunc callback, int buttonsCount);
        
        [DllImport("__Internal")]
        private static extern void _ShowMultipleeButtonsActionSheetView(string title, string message, string[] buttonsTexts, int[] buttonStyles, intCallbackFunc callback, int buttonsCount);
    
        #endif
        
        private static Queue<AlertData> alertQueue = new Queue<AlertData>();
        private static bool isShowing = false;
        
        public static void ShowAlertView(AlertData data)
        {
            //There should be at leaste one button
            if(data.buttons.Count == 0)
                data.buttons.Add(new ButtonData("Ok", ButtonStyle.Default, null));

            CheckDestructive(data);
            
            //Add To Queue and start
            alertQueue.Enqueue(data);
            CheckAndStartAlert();
        }

        private static void CheckAndStartAlert()
        {
            Debug.Log("Is Showing - " + isShowing + "; Alerts Count - " + alertQueue.Count);
            
            if (isShowing || alertQueue.Count == 0)
                return;
            
            switch (alertQueue.Peek().style)
            {
                case AlertStyle.None:
                    ShowAlertStyleView(alertQueue.Peek());
                    break;
                case AlertStyle.Alert:
                    Debug.Log("Show Alert " + alertQueue.Peek().title);
                    ShowAlertStyleView(alertQueue.Peek());
                    break;
                case AlertStyle.ActionSheet:
                    Debug.Log("Show Action Sheet" + alertQueue.Peek().title);
                    ShowActionSheetStyleView(alertQueue.Peek());
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            isShowing = true;
        }
        
        private static void ShowAlertStyleView(AlertData data)
        {
            #if UNITY_IOS && !UNITY_EDITOR
            
            _ShowMultipleeButtonsAlertView(
                data.title,
                data.message,
                data.buttons.Select(b => b.title).ToArray(),
                data.buttons.Select(b => (int)b.style).ToArray(),
                ButtonClickedHandler,
                data.buttons.Count
                );
    
            #else
            Debug.LogError("You try to use Alerts but they available only for iOS!");
            #endif
        }
        
        private static void ShowActionSheetStyleView(AlertData data)
        {
            #if UNITY_IOS && !UNITY_EDITOR
            
            _ShowMultipleeButtonsActionSheetView(
                data.title,
                data.message,
                data.buttons.Select(b => b.title).ToArray(),
                data.buttons.Select(b => (int)b.style).ToArray(),
                ButtonClickedHandler,
                data.buttons.Count
            );
    
            #else
            Debug.LogError("You try to use Alerts but they available only for iOS!");
            #endif
        }
        
        #if UNITY_IOS && !UNITY_EDITOR
        
        [MonoPInvokeCallback(typeof(intCallbackFunc))]
        private static void ButtonClickedHandler(int number)
        {
            var alert = alertQueue.Dequeue();
            alert.buttons[number].callback.Invoke();
            
            isShowing = false;
            CheckAndStartAlert();
        }
    
        #endif

        private static void CheckDestructive(AlertData data)
        {
            var destructiveCount = 0;
            foreach (var button in data.buttons)
            {
                if (button.style == ButtonStyle.Destructive)
                    destructiveCount++;
            }
            
            if(destructiveCount > 1)
                throw new Exception("There could not be more than one Destructive button!");
        }
    }
}