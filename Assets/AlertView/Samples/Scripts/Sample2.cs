﻿using System.Collections.Generic;
using AlertView.iOS;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Sample2 : MonoBehaviour
{
    public Text DebugText;
    public Button TestButton;
    public Button NextSampleBtn;
    
    
    private void Awake()
    {
        TestButton.onClick.AddListener(StartAlertsOneByOne);
        
        NextSampleBtn.onClick.AddListener(() => {SceneManager.LoadScene("Sample1");});
    }

    private void StartAlertsOneByOne()
    {
        StartFirstAlert();
        StartSecondAlert();
        StartThirdAlert();
    }

    private void StartFirstAlert()
    {   
        var alert = new AlertData(
            "This is title of first alert",
            "This is an example of how Alerts Queue works. This is first alert message.",
            AlertStyle.Alert,
            new List<ButtonData>()
            {
                new ButtonData("Ok", ButtonStyle.Default, CallbackHandlerFirst),
            }
        );
        
        AlertViewManager.ShowAlertView(alert);
    }

    private void CallbackHandlerFirst()
    {
        DebugText.text = "First Alert Callback!";
    }
    
    private void StartSecondAlert()
    {
        var alert = new AlertData(
            "This is title of second alert",
            "This is second alert message.",
            AlertStyle.Alert,
            new List<ButtonData>()
            {
                new ButtonData("Ok", ButtonStyle.Default, CallbackHandlerSecond),
            }
        );
        
        AlertViewManager.ShowAlertView(alert);
    }

    private void CallbackHandlerSecond()
    {
        DebugText.text = "Second Alert Callback!";
    }
    
    private void StartThirdAlert()
    {
        var alert = new AlertData(
            "This is title of third alert",
            "This is third alert message. It will be last one.",
            AlertStyle.Alert,
            new List<ButtonData>()
            {
                new ButtonData("Ok", ButtonStyle.Default, CallbackHandlerThird),
            }
        );
        
        AlertViewManager.ShowAlertView(alert);
    }

    private void CallbackHandlerThird()
    {
        DebugText.text = "Third Alert Callback!";
    }
}